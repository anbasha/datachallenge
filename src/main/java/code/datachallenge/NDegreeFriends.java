package code.datachallenge;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.flink.api.common.functions.RichGroupReduceFunction;
import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.IterativeDataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.apache.flink.util.Collector;

/**
 * Class for computation of i-th degree friends in the social network.
 */
public class NDegreeFriends {
	
	public static void main(String[] args) throws Exception {
		
		ParameterTool params = ParameterTool.fromArgs(args);
		
		int degree = 0;
		try {
			degree = params.getInt("degree");
			if (degree < 1) {
				throw new IllegalArgumentException();
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Invalid value of --degree parameter: " + degree + ". Must be > 1");
			System.exit(0);
		} catch (Exception e) {
			System.out.println("Value of --degree parameter is missing");
			System.exit(0);
		}
		
		String input = params.get("input");
		
		if (input == null) {
			System.out.println("Value of --input parameter is missing");
			System.exit(0);
		} 
		
		String output = params.get("output");
		if (output == null) {
			System.out.println("Value of --output parameter is missing");
			System.exit(0);
		}
		
		run(degree, input, output);
	}
	
	/**
	 * Runs the distributed computation of i-th degree friends.
	 * 
	 * @param degree 
	 * 			Degree of friendship to be computed
	 * @param input 
	 * 			Input file/folder path
	 * @param output 
	 * 			Output file path
	 * @throws Exception 
	 * 			Thrown on Flink Runtime failure
	 */
	public static void run(int degree, String input, String output) throws Exception {
		
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		
		//Read delimited file
		DataSet<Tuple2<String, String>> edges = env.readCsvFile(input)
													.lineDelimiter("\n")
													.fieldDelimiter("\t")
													.types(String.class, String.class);
		
		//Unroll the pairs and set the initial friendship degrees to 1:
		// (davidbowie, omid) -> (davidbowie, omid, 1), (omid, davidbowie, 1)
		DataSet<Tuple3<String, String, Integer>> initialDegrees = edges
			.flatMap((Tuple2<String, String> tuple, Collector<Tuple3<String, String, Integer>> out) -> {
					out.collect(new Tuple3<String, String, Integer>(tuple.f0, tuple.f1, 1));
					out.collect(new Tuple3<String, String, Integer>(tuple.f1, tuple.f0, 1));
				}
			);
		
		//Initiate the iterative part of data flow. Maximum number of iterations required is ceiling(log2(degree))
		IterativeDataSet<Tuple3<String, String, Integer>> solutionEdges 
						= initialDegrees.iterate((int)Math.ceil(Math.log(degree)/Math.log(2)));
		
		//Each iteration:
		DataSet<Tuple3<String, String, Integer>> newSolutionSet = solutionEdges
			//Group the pairs by first field
			.groupBy(0)
			//Reduce each group
			.reduceGroup(new RichGroupReduceFunction<Tuple3<String,String,Integer>, Tuple3<String,String,Integer>>() {

				private static final long serialVersionUID = 1L;
				
				private int iteration = 1;

				/**
				 * Called on each new iteration. Keeps the state
				 * throughout the iterative execution.
				 * This allows us to keep the number of the current iteration.
				 */
				@Override
				public void open(Configuration parameters) {
					iteration++;
				}

				/**
				 * For each set of pairs produces the possible new combinations and new friendship degrees.
				 * (torsten, omid, 1), (torsten, kim, 1) -> (omid, kim, 2), (kim, omid, 2)
				 */
				public void reduce(
						Iterable<Tuple3<String, String, Integer>> tuples,
						Collector<Tuple3<String, String, Integer>> out)
						throws Exception {
					
					HashMap<String, Integer> degrees = new HashMap<String, Integer>();
					
					Iterator<Tuple3<String, String, Integer>> iterator = tuples.iterator();
					
					while (iterator.hasNext()) {
						Tuple3<String, String, Integer> friend1 = iterator.next();
						
						//Keep current pair in the output
						out.collect(friend1);
						
						for (String friend2 : degrees.keySet()) {
							int friendshipDegree = degrees.get(friend2) + friend1.f2;
							
							//The check (friendshipDegree <= degree) allows us to avoid producing the pairs of degree higher 
							//than required by user. The check (friendshipDegree >= iteration) allows to avoid duplicate entries.
							//For example, if current iteration number is 4, this means that all friendships of degree 2 and 3
							//were already produced on previous iterations and we don't need to do it again.
							if (friendshipDegree <= degree && friendshipDegree >= iteration) {
								out.collect(new Tuple3<String, String, Integer>(friend1.f1, friend2, friendshipDegree));
								out.collect(new Tuple3<String, String, Integer>(friend2, friend1.f1, friendshipDegree));
							}
						}
						degrees.put(friend1.f1, friend1.f2);
					}
				}
			})
			//Grouping by both friends and minimizing by second field allows to
			//avoid duplicates in the output caused by the fact that there can
			//be multiple paths between two friends.
			.groupBy(0, 1)
			.min(2);
		
		//Feed the result of this iteration to the head of iterative data flow.
		//Variable finalPairs will hold the result of all the iterations. Project it on first two
		//fields as we are not interested in degrees anymore.
		DataSet<Tuple2<String, String>> finalPairs = solutionEdges.closeWith(newSolutionSet).project(0, 1);
		
		//Group the pairs by the first field and reduce it to one
		//final line containing the name and the sorted list of friends.
		DataSet<Tuple2<String, String>> result = finalPairs
			.groupBy(0)
			.sortGroup(1, Order.ASCENDING)
			.reduceGroup((Iterable<Tuple2<String, String>> pairs,
						Collector<Tuple2<String, String>> out) -> {
					
					Iterator<Tuple2<String, String>> iterator = pairs.iterator();
					Tuple2<String, String> firstElement = iterator.next();
					String key = firstElement.f0;
					StringBuilder sb = new StringBuilder(firstElement.f1);
					
					while (iterator.hasNext()) {
						sb.append("\t").append(iterator.next().f1);
					}
					
					out.collect(new Tuple2<String, String>(key, sb.toString()));
				}
			);
		
		//This part is sorting the result globally. This is costly (the parallelism needs
		//to be set to 1 to send all the results to one node)
		//but required by task.
		result.partitionByRange(0).sortPartition(0, Order.ASCENDING).setParallelism(1)
			.map((Tuple2<String, String> in) -> {
					return in.f0 + "\t" + in.f1;
				}
			).writeAsText(output, WriteMode.OVERWRITE);
		
		env.execute();
	}
}