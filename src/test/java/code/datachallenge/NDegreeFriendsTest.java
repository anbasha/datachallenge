package code.datachallenge;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class NDegreeFriendsTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	@Test
	public void testDataPipeline() throws Exception {
		
		List<String> inputLines = Arrays.asList(new String[] {
				"davidbowie" + "\t" + "omid",
				"davidbowie" + "\t" + "kim",
				"kim" + "\t" + "torsten",
				"torsten" + "\t" + "omid",
				"brendan" + "\t" + "torsten",
				"ziggy" + "\t" + "davidbowie",
				"mick" + "\t" + "ziggy"
				});
		
		List<String> expectedOutput = Arrays.asList(new String[] {
				"brendan" + "\t" + "kim" + "\t" + "omid" + "\t" + "torsten",
				"davidbowie" + "\t" + "kim" + "\t" + "mick" + "\t" + "omid" + "\t" + "torsten" + "\t" + "ziggy",
				"kim" + "\t" + "brendan" + "\t" + "davidbowie" + "\t" + "omid" + "\t" + "torsten" + "\t" + "ziggy",
				"mick" + "\t" + "davidbowie" + "\t" + "ziggy",
				"omid" + "\t" + "brendan" + "\t" + "davidbowie" + "\t" + "kim" + "\t" + "torsten" + "\t" + "ziggy",
				"torsten" + "\t" + "brendan" + "\t" + "davidbowie" + "\t" + "kim" + "\t" + "omid",
				"ziggy" + "\t" + "davidbowie" + "\t" + "kim" + "\t" + "mick" + "\t" + "omid"
		});
		
		File inputFile = tempFolder.newFile("input");
		File outputFile = tempFolder.newFile("output");
		
		FileUtils.writeLines(inputFile, inputLines);
		
		NDegreeFriends.run(2, inputFile.getAbsolutePath(), outputFile.getAbsolutePath());
		
		List<String> outputLines = FileUtils.readLines(outputFile);
		
		assertEquals(expectedOutput, outputLines);
	}
}
