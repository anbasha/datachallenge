Data Challenge Implementation
=============================

The details of implementation are discussed in the comments of the code,
the analysis of the complexity and correctness is discussed [here](notes.pdf).
 
In order to build this project maven dependency management tool is required.

In the root directory of the project execute:

`mvn clean compile assembly:single`

This produces the jar file with dependencies: `target/datachallenge-1.0.0-jar-with-dependencies.jar`

In order to run the jar file, you need Java 8. To run the jar execute:

`java -jar datachallenge-1.0.0-jar-with-dependencies.jar  --input /path/to/input/folder/or/file --output /path/to/output --degree 2`

There are 3 parameters you need to pass:

* input - path to input folder or file
* output - path to output file
* degree - the degree of friendship to be computed.

The result will be written to the output file.

